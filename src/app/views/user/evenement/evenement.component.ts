import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { EventsService } from 'src/app/service/events.service';

@Component({
  selector: 'app-evenement',
  templateUrl: './evenement.component.html',
  styleUrls: ['./evenement.component.css']
})
export class EvenementComponent implements OnInit {
  list:any;
    constructor(private service:EventsService  ,private router:Router) {

     }

    ngOnInit(): void {
      this.getall();


    }

    getall():void{

  this.service.getAll().subscribe({next: (data) => {

  this.list= data;

  console.log(data);

  },

  error: (e) => console.error(e)

  });

    }


  }

