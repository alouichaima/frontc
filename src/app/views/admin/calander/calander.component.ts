import { Component, OnInit, ViewChild } from '@angular/core';
import {
  setOptions,
  MbscEventcalendarView,
  MbscCalendarEvent,
  localeFr,
} from '@mobiscroll/angular';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

setOptions({
  locale: localeFr,
  theme: 'windows',
  themeVariant: 'light',
  clickToCreate: true,
  dragToCreate: true,
  dragToMove: true,
  dragToResize: true,
});

@Component({
  selector: 'app-calander',
  templateUrl: './calander.component.html',
  styleUrls: ['./calander.component.css'],
})
export class CalanderComponent implements OnInit {
  dateSeance: any;
  nomSeance: any;

  @ViewChild('popupForm') popupForm!: any;
  myPopupOptions: any = {
    scrollLock: false,
    height: 400,
    width: 600,
  };
  router: any;

  constructor(private http: HttpClient) {}

  myEvents: MbscCalendarEvent[] = [];
  view = 'schedule';
  calView: MbscEventcalendarView = {
    schedule: { type: 'week' },
  };
  currentDate: any = new Date();

  ngOnInit(): void {
    this.http.get('http://localhost:8087/seance/all').subscribe((res: any) => {
      res.forEach((seance: any) => {



        let tmpEvent: MbscCalendarEvent = {

           color: "#ff6d42",
      start: new Date(seance.datedebut),
      end:  new Date(seance.datefin),
      id: "mbsc_1",
      title: seance.nomS,
        };
        this.myEvents = [ ...this.myEvents, tmpEvent]

      });
    });


  }
  guid = 1;

  submitSeance() {
    this.http
      .post('http://localhost:8087/seance', {
        nomS: this.nomSeance,
        datedebut: this.dateSeance[0],
        datefin: this.dateSeance[1],
      })
      .subscribe((res: any) => {
        this.myEvents = [
          {
            color: '#ff6d42',
            start: this.dateSeance[0],
            end: this.dateSeance[1],
            id: 'mbsc_' + Math.random(),
            title: this.nomSeance,
          },
        ];
        console.log(res);
      });
  }

  addEvent(e: any) {
    console.log(this.popupForm);
    this.popupForm.open();

    /**
     * nom seance
     * date debut
     * date fin
     *
     */

    //   this.myEvents = [
    // {
    //   color: "#ff6d42",
    //   start: "2022-04-25T07:00:00.000Z",
    //   end: "2022-04-28T16:00:00.000Z",
    //   id: "mbsc_1",
    //   title: "Business of Software Conference",
    // }
    //   ]
  }

  changeView(): void {
    setTimeout(() => {
      switch (this.view) {
        case 'calendar':
          this.calView = {
            calendar: { labels: true },
          };
          break;
        case 'schedule':
          this.calView = {
            schedule: { type: 'week' },
          };
          break;
      }
    });
  }

  getFirstDayOfWeek(d: Date, prev: boolean): Date {
    const day = d.getDay();
    const diff = d.getDate() - day + (prev ? -7 : 7);
    return new Date(d.setDate(diff));
  }

  navigatePage(prev: boolean): void {
    const currentDate = this.currentDate;
    if (this.view === 'calendar') {
      const prevNextPage = new Date(
        currentDate.getFullYear(),
        currentDate.getMonth() + (prev ? -1 : 1),
        1
      );
      this.currentDate = prevNextPage;
    } else {
      const prevNextSunday = this.getFirstDayOfWeek(currentDate, prev);
      this.currentDate = prevNextSunday;
    }
  }
}
