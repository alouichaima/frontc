import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserCRUDService } from 'src/app/service/user-crud.service';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {
  url:any ="";
  us: any={'nom':'','prenom':'', 'datenaiss':'', 'email':'', 'password':'', 'telephone':'', 'poids':''};

  constructor(private serviceuser:UserCRUDService, private router:Router) { }

  ngOnInit(): void {
  }

  add(){
    console.log(this.us);
    this.serviceuser.adduser(this.us).subscribe({

       next: (data:any)=>{
         this.router.navigate (['admin/listespace'])

      },

      error: (e:any)=> console.error(e),

      complete:()=>{}

      })


}

}
