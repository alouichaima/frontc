import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CoachService } from 'src/app/service/coach.service';

@Component({
  selector: 'app-addcoach',
  templateUrl: './addcoach.component.html',
  styleUrls: ['./addcoach.component.css']
})
export class AddcoachComponent implements OnInit {
  url:any ="";


  coa: any={'nom_prenom':'','description':'', 'image':""};
  constructor( private service:CoachService, private router:Router)
  { }

  ngOnInit(): void {
  }

  add(){
    console.log(this.coa);
    this.service.addcoach(this.coa).subscribe({

       next: (data:any)=>{
         this.router.navigate (['admin/listcoach'])

      },

      error: (e:any)=> console.error(e),

      complete:()=>{}

      })


}

}
