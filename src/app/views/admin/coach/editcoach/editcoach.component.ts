import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CoachService } from 'src/app/service/coach.service';

@Component({
  selector: 'app-editcoach',
  templateUrl: './editcoach.component.html',
  styleUrls: ['./editcoach.component.css']
})
export class EditcoachComponent implements OnInit {

  id:number =0;
  coa:any={'nomPrenom':'','description':'','image':'',compteF:'',compteI:''};

  constructor(private service:CoachService, private router:Router ,private route: ActivatedRoute ) {
    this.route.queryParams.subscribe(params=>{

      if(params&&params.special){

      this.coa=JSON.parse(params.special);



      }

      })
   }
  ngOnInit(): void {


  }
  onSubmit(){

  }

  retour():void{

    this.router.navigate(['admin/listcoach']);

  }
  modif():void{
    this.service.update(this.coa).subscribe({

      next: (data:any)=>{
        this.router.navigate (['admin/listcoach'])

     },

     error: (c:any)=> console.error(c),

     complete:()=>{}

     })

  }

}
