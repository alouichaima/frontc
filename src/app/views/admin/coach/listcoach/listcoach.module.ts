import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListcoachRoutingModule } from './listcoach-routing.module';
import { ListcoachComponent } from './listcoach.component';


@NgModule({
  declarations: [
    ListcoachComponent
  ],
  imports: [
    CommonModule,
    ListcoachRoutingModule
  ]
})
export class ListcoachModule { }
