import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddtarifRoutingModule } from './addtarif-routing.module';
import { AddtarifComponent } from './addtarif.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AddtarifComponent
  ],
  imports: [
    CommonModule,
    AddtarifRoutingModule,
    FormsModule
  ]
})
export class AddtarifModule { }
