import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListetarifComponent } from './listetarif.component';

describe('ListetarifComponent', () => {
  let component: ListetarifComponent;
  let fixture: ComponentFixture<ListetarifComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListetarifComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListetarifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
