import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListeactRoutingModule } from './listeact-routing.module';
import { ListeactComponent } from './listeact.component';


@NgModule({
  declarations: [
    ListeactComponent
  ],
  imports: [
    CommonModule,
    ListeactRoutingModule
  ]
})
export class ListeactModule { }
