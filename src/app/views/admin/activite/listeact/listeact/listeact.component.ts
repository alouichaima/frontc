import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Route, Router } from '@angular/router';
import { ActiviteCrudService } from 'src/app/service/activite-crud.service';

@Component({
  selector: 'app-listeact',
  templateUrl: './listeact.component.html',
  styleUrls: ['./listeact.component.css']
})
export class ListeactComponent implements OnInit {
  listeact: any;

  constructor(private serviceact:ActiviteCrudService,private router:Router) { }

  ngOnInit(): void {
    this.getall();
    this.deleteact;
  }

  getall():void{

    this.serviceact.getAll().subscribe({next: (data) => {
    
    this.listeact= data;
    
    console.log(data);
    
    },
    
    error: (a) => console.error(a)
    
    });
    
      }
      deleteact(id:number){
        this.serviceact. supprimer(id)
          .subscribe(data => {
            this.deleteact=data;
            console.log(data);
          },
            error => console.log(error));
    
      }
      update(a:any){
        this.router.navigate(['admin/editact' ,a]);
        let navigationExtras:NavigationExtras={
    
          queryParams:{
    
          special:JSON.stringify(a)
    
          }
    
          }
    
          this.router.navigate(['admin/editact'],navigationExtras);
    
      }

}
