import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Coach } from '../models/coach';

@Injectable({
  providedIn: 'root'
})
export class CoachService {

  private baseUrl = 'http://localhost:8087/coach';
  constructor(private http:HttpClient) { }

   // tslint:disable-next-line:typedef
   getAll()
   {
     return this.http.get(this.baseUrl + '/all');
   }

   addcoach(c:Coach):Observable<object>{
    return this.http.post("http://localhost:8087/coach" ,c ).pipe()

  }

  getCoachById(id: number): Observable<any>
  {
    return this.http.get(this.baseUrl  + id);
  }

  supprimer(id: number): Observable<any>
  {
    return this.http.delete(this.baseUrl +  "/" +id, { responseType: 'text' });
  }
   update(coa:Coach): any
   {
     return this.http.put(this.baseUrl ,coa).pipe();
   }
}
