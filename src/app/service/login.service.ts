import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

const AUTH_API = 'http://localhost:8087/api/auth';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient , private route:Router) { }

  login(credentials:any): Observable<any> {
    return this.http.post(AUTH_API + '/login', {
      email:credentials.email,
      password:credentials.password
    }, httpOptions);
  }


  register(user:any): Observable<any> {
    return this.http.post(AUTH_API + '/register', {
      nom: user.nom,
      prenom: user.prenom,
      datenaiss: user.datenaiss,
      email: user.email,
      password:user.password,
      telephone: user.telephone,
      poids: user.poids,
    }, httpOptions);
  }

  sendEmail( user:any) {
    return this.http.get(AUTH_API + '/send_email',user)

}

}
