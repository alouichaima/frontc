import { TestBed } from '@angular/core/testing';

import { ActiviteCrudService } from './activite-crud.service';

describe('ActiviteCrudService', () => {
  let service: ActiviteCrudService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ActiviteCrudService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
