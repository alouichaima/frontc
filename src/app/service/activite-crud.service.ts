import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { activite } from '../models/activite.model';


@Injectable({
  providedIn: 'root'
})
export class ActiviteCrudService {
  private baseUrl = 'http://localhost:8087/activite';

  constructor(private http:HttpClient) { }

  // tslint:disable-next-line:typedef
  getAll()
  {
    return this.http.get(this.baseUrl + '/all');
  }

  addact(a:activite):Observable<object>{
    return this.http.post("http://localhost:8087/activite" ,a ).pipe()

  }

 getactById(id: number): Observable<any>
  {
    return this.http.get(this.baseUrl  + id);
  }

  supprimer(id: number): Observable<any>
  {
    return this.http.delete(this.baseUrl +  "/" +id, { responseType: 'text' });
  }
  update(act:activite): any
  {
    return this.http.put(this.baseUrl ,act).pipe();
  }
}
