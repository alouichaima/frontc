import { Coach } from './coach';
import { activite } from './activite.model';
import { Espace } from './espace';
export class Seance{
  idS?:number;
  nomS?:string;
  datedebut?:Date;
  datefin?:Date;
  Espace?:Espace;
  activite?:activite;
  Coach?:Coach;

}
